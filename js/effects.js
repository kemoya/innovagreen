$(document).ready(function() {

    /** CLICK button menu **/

    $("#btncollapse").click(function() {
        $("#collapsibleNavbar").toggle();
    });
    
    /** CLICK SECTION 1 **/

    $("#linksection1").click(function() {
        $("#collapsibleNavbar").hide();
    });

    /** CLICK SECTION 2 **/

    $("#linksection2").click(function() {
        $("#solution1").hide();
        $("#solution2").hide();
        $("#solution3").hide();
        $("#solution4").hide();
        $("#solution5").hide();
        $("#solution1P").css("opacity", "1");
        $("#solution2P").css("opacity", "1");
        $("#solution3P").css("opacity", "1");
        $("#solution4P").css("opacity", "1");
        $("#solution5P").css("opacity", "1");
        $("#collapsibleNavbar").hide();
    });

    /** Unfold solutions sections + Opacity clicked**/

    $("#solution1P").click(function() {

        $("#solution1").toggle();
        $("#solution2").hide();
        $("#solution3").hide();
        $("#solution4").hide();
        $("#solution5").hide();

        $("#solution1P").css("opacity", "1");
        $("#solution2P").css("opacity", "0.5");
        $("#solution3P").css("opacity", "0.5");
        $("#solution4P").css("opacity", "0.5");
        $("#solution5P").css("opacity", "0.5");
    });

    $("#solution2P").click(function() {
        $("#solution2").toggle();
        $("#solution1").hide();
        $("#solution3").hide();
        $("#solution4").hide();
        $("#solution5").hide();

        $("#solution1P").css("opacity", "0.5");
        $("#solution2P").css("opacity", "1");
        $("#solution3P").css("opacity", "0.5");
        $("#solution4P").css("opacity", "0.5");
        $("#solution5P").css("opacity", "0.5");
        $("#solution6P").css("opacity", "0.5");
    });

    $("#solution3P").click(function() {
        $("#solution3").toggle();
        $("#solution2").hide();
        $("#solution1").hide();
        $("#solution4").hide();
        $("#solution5").hide();

        $("#solution1P").css("opacity", "0.5");
        $("#solution2P").css("opacity", "0.5");
        $("#solution3P").css("opacity", "1");
        $("#solution4P").css("opacity", "0.5");
        $("#solution5P").css("opacity", "0.5");
    });

    $("#solution4P").click(function() {
        $("#solution4").toggle();
        $("#solution2").hide();
        $("#solution3").hide();
        $("#solution1").hide();
        $("#solution5").hide();

        $("#solution1P").css("opacity", "0.5");
        $("#solution2P").css("opacity", "0.5");
        $("#solution3P").css("opacity", "0.5");
        $("#solution4P").css("opacity", "1");
        $("#solution5P").css("opacity", "0.5");
    });

    $("#solution5P").click(function() {
        $("#solution5").toggle();
        $("#solution2").hide();
        $("#solution3").hide();
        $("#solution4").hide();
        $("#solution1").hide();

        $("#solution1P").css("opacity", "0.5");
        $("#solution2P").css("opacity", "0.5");
        $("#solution3P").css("opacity", "0.5");
        $("#solution4P").css("opacity", "0.5");
        $("#solution5P").css("opacity", "1");
    });


    /** HOVER ICONS **/

    $("#icon1").hover(function() {
        $("#h51").css("-ms-transform", "scale(1.5)");
        $("#h51").css("-webkit-transform", "scale(1.5)");
        $("#h51").css("transform", "scale(1.5)");
        $("#h51").css("margin-top", "2.5rem");
    }, function() {
        $("#h51").css("-ms-transform", "scale(1)");
        $("#h51").css("-webkit-transform", "scale(1)");
        $("#h51").css("transform", "scale(1)");
        $("#h51").css("margin-top", "1rem");
    });

    $("#icon2").hover(function() {
        $("#h52").css("-ms-transform", "scale(1.5)");
        $("#h52").css("-webkit-transform", "scale(1.5)");
        $("#h52").css("transform", "scale(1.5)");
        $("#h52").css("margin-top", "2.5rem");
    }, function() {
        $("#h52").css("-ms-transform", "scale(1)");
        $("#h52").css("-webkit-transform", "scale(1)");
        $("#h52").css("transform", "scale(1)");
        $("#h52").css("margin-top", "1rem");
    });

    $("#icon3").hover(function() {
        $("#h53").css("-ms-transform", "scale(1.5)");
        $("#h53").css("-webkit-transform", "scale(1.5)");
        $("#h53").css("transform", "scale(1.5)");
        $("#h53").css("margin-top", "2.5rem");
    }, function() {
        $("#h53").css("-ms-transform", "scale(1)");
        $("#h53").css("-webkit-transform", "scale(1)");
        $("#h53").css("transform", "scale(1)");
        $("#h53").css("margin-top", "1rem");
    });

    $("#icon4").hover(function() {
        $("#h54").css("-ms-transform", "scale(1.5)");
        $("#h54").css("-webkit-transform", "scale(1.5)");
        $("#h54").css("transform", "scale(1.5)");
        $("#h54").css("margin-top", "2.5rem");
    }, function() {
        $("#h54").css("-ms-transform", "scale(1)");
        $("#h54").css("-webkit-transform", "scale(1)");
        $("#h54").css("transform", "scale(1)");
        $("#h54").css("margin-top", "1rem");
    });

    $("#icon5").hover(function() {
        $("#h55").css("-ms-transform", "scale(1.5)");
        $("#h55").css("-webkit-transform", "scale(1.5)");
        $("#h55").css("transform", "scale(1.5)");
        $("#h55").css("margin-top", "2.5rem");
    }, function() {
        $("#h55").css("-ms-transform", "scale(1)");
        $("#h55").css("-webkit-transform", "scale(1)");
        $("#h55").css("transform", "scale(1)");
        $("#h55").css("margin-top", "1rem");
    });

    $("#icon6").hover(function() {
        $("#h56").css("-ms-transform", "scale(1.5)");
        $("#h56").css("-webkit-transform", "scale(1.5)");
        $("#h56").css("transform", "scale(1.5)");
        $("#h56").css("margin-top", "2.5rem");
    }, function() {
        $("#h56").css("-ms-transform", "scale(1)");
        $("#h56").css("-webkit-transform", "scale(1)");
        $("#h56").css("transform", "scale(1)");
        $("#h56").css("margin-top", "1rem");
    });

    $("#icon7").hover(function() {
        $("#h57").css("-ms-transform", "scale(1.5)");
        $("#h57").css("-webkit-transform", "scale(1.5)");
        $("#h57").css("transform", "scale(1.5)");
        $("#h57").css("margin-top", "2.5rem");
    }, function() {
        $("#h57").css("-ms-transform", "scale(1)");
        $("#h57").css("-webkit-transform", "scale(1)");
        $("#h57").css("transform", "scale(1)");
        $("#h57").css("margin-top", "1rem");
    });

    $("#icon8").hover(function() {
        $("#h58").css("-ms-transform", "scale(1.5)");
        $("#h58").css("-webkit-transform", "scale(1.5)");
        $("#h58").css("transform", "scale(1.5)");
        $("#h58").css("margin-top", "2.5rem");
    }, function() {
        $("#h58").css("-ms-transform", "scale(1)");
        $("#h58").css("-webkit-transform", "scale(1)");
        $("#h58").css("transform", "scale(1)");
        $("#h58").css("margin-top", "1rem");
    });

    $("#icon9").hover(function() {
        $("#h59").css("-ms-transform", "scale(1.5)");
        $("#h59").css("-webkit-transform", "scale(1.5)");
        $("#h59").css("transform", "scale(1.5)");
        $("#h59").css("margin-top", "2.5rem");
    }, function() {
        $("#h59").css("-ms-transform", "scale(1)");
        $("#h59").css("-webkit-transform", "scale(1)");
        $("#h59").css("transform", "scale(1)");
        $("#h59").css("margin-top", "1rem");
    });

    $("#icon10").hover(function() {
        $("#h510").css("-ms-transform", "scale(1.5)");
        $("#h510").css("-webkit-transform", "scale(1.5)");
        $("#h510").css("transform", "scale(1.5)");
        $("#h510").css("margin-top", "2.5rem");
    }, function() {
        $("#h510").css("-ms-transform", "scale(1)");
        $("#h510").css("-webkit-transform", "scale(1)");
        $("#h510").css("transform", "scale(1)");
        $("#h510").css("margin-top", "1rem");
    });

    $("#icon11").hover(function() {
        $("#h511").css("-ms-transform", "scale(1.5)");
        $("#h511").css("-webkit-transform", "scale(1.5)");
        $("#h511").css("transform", "scale(1.5)");
        $("#h511").css("margin-top", "2.5rem");
    }, function() {
        $("#h511").css("-ms-transform", "scale(1)");
        $("#h511").css("-webkit-transform", "scale(1)");
        $("#h511").css("transform", "scale(1)");
        $("#h511").css("margin-top", "1rem");
    });

    $("#icon12").hover(function() {
        $("#h512").css("-ms-transform", "scale(1.5)");
        $("#h512").css("-webkit-transform", "scale(1.5)");
        $("#h512").css("transform", "scale(1.5)");
        $("#h512").css("margin-top", "2.5rem");
    }, function() {
        $("#h512").css("-ms-transform", "scale(1)");
        $("#h512").css("-webkit-transform", "scale(1)");
        $("#h512").css("transform", "scale(1)");
        $("#h512").css("margin-top", "1rem");
    });

    $("#icon13").hover(function() {
        $("#h513").css("-ms-transform", "scale(1.5)");
        $("#h513").css("-webkit-transform", "scale(1.5)");
        $("#h513").css("transform", "scale(1.5)");
        $("#h513").css("margin-top", "2.5rem");
    }, function() {
        $("#h513").css("-ms-transform", "scale(1)");
        $("#h513").css("-webkit-transform", "scale(1)");
        $("#h513").css("transform", "scale(1)");
        $("#h513").css("margin-top", "1rem");
    });

    $("#icon14").hover(function() {
        $("#h514").css("-ms-transform", "scale(1.5)");
        $("#h514").css("-webkit-transform", "scale(1.5)");
        $("#h514").css("transform", "scale(1.5)");
        $("#h514").css("margin-top", "2.5rem");
    }, function() {
        $("#h514").css("-ms-transform", "scale(1)");
        $("#h514").css("-webkit-transform", "scale(1)");
        $("#h514").css("transform", "scale(1)");
        $("#h514").css("margin-top", "1rem");
    });

    $("#icon15").hover(function() {
        $("#h515").css("-ms-transform", "scale(1.5)");
        $("#h515").css("-webkit-transform", "scale(1.5)");
        $("#h515").css("transform", "scale(1.5)");
        $("#h515").css("margin-top", "2.5rem");
    }, function() {
        $("#h515").css("-ms-transform", "scale(1)");
        $("#h515").css("-webkit-transform", "scale(1)");
        $("#h515").css("transform", "scale(1)");
        $("#h515").css("margin-top", "1rem");
    });

    $("#icon16").hover(function() {
        $("#h516").css("-ms-transform", "scale(1.5)");
        $("#h516").css("-webkit-transform", "scale(1.5)");
        $("#h516").css("transform", "scale(1.5)");
        $("#h516").css("margin-top", "2.5rem");
    }, function() {
        $("#h516").css("-ms-transform", "scale(1)");
        $("#h516").css("-webkit-transform", "scale(1)");
        $("#h516").css("transform", "scale(1)");
        $("#h516").css("margin-top", "1rem");
    });

    $("#icon17").hover(function() {
        $("#h517").css("-ms-transform", "scale(1.5)");
        $("#h517").css("-webkit-transform", "scale(1.5)");
        $("#h517").css("transform", "scale(1.5)");
        $("#h517").css("margin-top", "2.5rem");
    }, function() {
        $("#h517").css("-ms-transform", "scale(1)");
        $("#h517").css("-webkit-transform", "scale(1)");
        $("#h517").css("transform", "scale(1)");
        $("#h517").css("margin-top", "1rem");
    });

    $("#icon18").hover(function() {
        $("#h518").css("-ms-transform", "scale(1.5)");
        $("#h518").css("-webkit-transform", "scale(1.5)");
        $("#h518").css("transform", "scale(1.5)");
        $("#h518").css("margin-top", "2.5rem");
    }, function() {
        $("#h518").css("-ms-transform", "scale(1)");
        $("#h518").css("-webkit-transform", "scale(1)");
        $("#h518").css("transform", "scale(1)");
        $("#h518").css("margin-top", "1rem");
    });

    $("#icon19").hover(function() {
        $("#h519").css("-ms-transform", "scale(1.5)");
        $("#h519").css("-webkit-transform", "scale(1.5)");
        $("#h519").css("transform", "scale(1.5)");
        $("#h519").css("margin-top", "2.5rem");
    }, function() {
        $("#h519").css("-ms-transform", "scale(1)");
        $("#h519").css("-webkit-transform", "scale(1)");
        $("#h519").css("transform", "scale(1)");
        $("#h519").css("margin-top", "1rem");
    });

    /** CLICK SECTION 3 **/

    $("#linksection3").click(function() {
        $("#proj1").css("opacity", "1");
        $("#proj2").css("opacity", "1");
        $("#proj3").css("opacity", "1");
        $("#proj4").css("opacity", "1");
        $("#demop1").hide();
        $("#demop2").hide();
        $("#demop3").hide();
        $("#demop4").hide();
        $("#collapsibleNavbar").hide();
    });

    $("#proj1").click(function() {
        $("#demop1").toggle();
        $("#demop2").hide();
        $("#demop3").hide();
        $("#demop4").hide();
    });

    $("#proj2").click(function() {
        $("#demop2").toggle();
        $("#demop1").hide();
        $("#demop3").hide();
        $("#demop4").hide();
    });

    $("#proj3").click(function() {
        $("#demop3").toggle();
        $("#demop2").hide();
        $("#demop1").hide();
        $("#demop4").hide();
    });

    $("#proj4").click(function() {
        $("#demop4").toggle();
        $("#demop2").hide();
        $("#demop3").hide();
        $("#demop1").hide();
    });

    /**  Opacity clicked US section **/

    $("#proj1").click(function() {
        $("#proj1").css("opacity", "1");
        $("#proj2").css("opacity", "0.5");
        $("#proj3").css("opacity", "0.5");
        $("#proj4").css("opacity", "0.5");
    });

    $("#proj2").click(function() {
        $("#proj2").css("opacity", "1");
        $("#proj1").css("opacity", "0.5");
        $("#proj3").css("opacity", "0.5");
        $("#proj4").css("opacity", "0.5");
    });

    $("#proj3").click(function() {
        $("#proj3").css("opacity", "1");
        $("#proj2").css("opacity", "0.5");
        $("#proj1").css("opacity", "0.5");
        $("#proj4").css("opacity", "0.5");
    });

    $("#proj4").click(function() {
        $("#proj4").css("opacity", "1");
        $("#proj2").css("opacity", "0.5");
        $("#proj3").css("opacity", "0.5");
        $("#proj1").css("opacity", "0.5");
    });

    /** CLICK SECTION 4 **/

    $("#linksection4").click(function() {
        $("#us1").css("opacity", "1");
        $("#us2").css("opacity", "1");
        $("#us3").css("opacity", "1");
        $("#demo1").hide();
        $("#demo2").hide();
        $("#demo3").hide();
        $("#collapsibleNavbar").hide();
    });

    $("#us1").click(function() {
        $("#demo1").toggle();
        $("#demo2").hide();
        $("#demo3").hide();
    });

    $("#us2").click(function() {
        $("#demo2").toggle();
        $("#demo1").hide();
        $("#demo3").hide();
    });

    $("#us3").click(function() {
        $("#demo3").toggle();
        $("#demo2").hide();
        $("#demo1").hide();
    });


    /**  Opacity clicked US section **/

    $("#us1").click(function() {
        $("#us1").css("opacity", "1");
        $("#us2").css("opacity", "0.5");
        $("#us3").css("opacity", "0.5");
    });

    $("#us2").click(function() {
        $("#us2").css("opacity", "1");
        $("#us1").css("opacity", "0.5");
        $("#us3").css("opacity", "0.5");
    });

    $("#us3").click(function() {
        $("#us3").css("opacity", "1");
        $("#us2").css("opacity", "0.5");
        $("#us1").css("opacity", "0.5");
    });

});